FROM python:3.11-slim

WORKDIR /src

# Install worker as a package
COPY worker_woyolo worker_woyolo
COPY pyproject.toml ./

RUN pip install . --no-cache-dir

CMD ["worker-woyolo"]
